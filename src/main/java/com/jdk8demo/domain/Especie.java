package com.jdk8demo.domain;

public enum Especie {
    TERRAQUEO,
    IRKEN,
    ROBOT
}
