package com.jdk8demo;

import com.jdk8demo.domain.Especie;
import com.jdk8demo.domain.Personaje;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PersonajeBuilder {

    private Random random = new Random();
    private String nombre;

    public List<Personaje> build(int size) {
        List<Personaje> personajes = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            nombre = "Personaje " + i;
            personajes.add(build());
        }
        return personajes;
    }

    public Personaje build() {
        Especie especie = Especie.values()[random.nextInt(Especie.values().length)];
        int edad = random.nextInt(150);
        int peso = random.nextInt(300);
        return new Personaje(nombre == null ? "Personaje" : nombre, especie, edad, peso);
    }

}
