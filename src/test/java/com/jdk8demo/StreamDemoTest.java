package com.jdk8demo;

import com.jdk8demo.domain.Personaje;
import java.util.Comparator;
import java.util.List;
import org.junit.Test;
import static com.jdk8demo.domain.Especie.*;
import org.junit.After;

public class StreamDemoTest {

    @After
    public void setup() {
        System.out.println("=================================================");
    }
    
    @Test
    public void test_listarExtraterrestres() {
        List<Personaje> personajes = new PersonajeBuilder().build(20);
        listarExtraterrestres(personajes);
    }
    
    @Test
    public void test_obtenerTerraqueosOrdenados() {
        List<Personaje> personajes = new PersonajeBuilder().build(20);
        List<Personaje> ordenados = obtenerTerraqueosOrdenadosPorEdad(personajes);
        for (Personaje personaje : ordenados) {
            System.out.println(personaje);
        }
    }
    
    @Test
    public void test_contarTerraqueos() {
        List<Personaje> personajes = new PersonajeBuilder().build(20);
        int count = contarTerraqueos(personajes);        
        System.out.printf("Terraqueos encontrados: %d%n", count);
    }    
    
    @Test
    public void test_hayRobots() {
        List<Personaje> personajes = new PersonajeBuilder().build(20);
        boolean b = hayRobots(personajes);
        System.out.printf("Hay robots: %b%n", b);
    }
    
    
    
    /***************************************************************************
     * 
     * IMPLEMENTACIONES
     * 
     ***************************************************************************/
    
    private void listarExtraterrestres(List<Personaje> personajes) {
        for (Personaje personaje : personajes) {
            if (personaje.getEspecie() == IRKEN) {
                System.out.println(personaje.toString());
            }
        }
    }
    
    private List<Personaje> obtenerTerraqueosOrdenadosPorEdad(List<Personaje> personajes) {
        personajes.sort(new Comparator<Personaje>() {
            @Override
            public int compare(Personaje o1, Personaje o2) {
                return o1.getEdad() - o2.getEdad();
            }
        });
        return personajes;
    }

    private int contarTerraqueos(List<Personaje> personajes) {
        int count = 0;
        for (Personaje personaje : personajes) {
            if (personaje.getEspecie() == TERRAQUEO) {
                count++;
            }
        }
        return count;
    }    
    
    private boolean hayRobots(List<Personaje> personajes) {
        for (Personaje personaje : personajes) {
            if (personaje.getEspecie() == ROBOT) {
                return true;
            }
        }
        return false;
    }    
}
